﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Controller
{

    public class PatrolController : MonoBehaviour
    {
        const float waypointSize = 0.3f;

        private void OnDrawGizmos() {
            int childrenCount = transform.childCount;
            for (int i = 0; i < childrenCount; i++) {

                int j = GetNextWaypointIndex(i);

                Gizmos.DrawSphere(GetWayPoint(i), waypointSize);
                Gizmos.DrawLine(GetWayPoint(i), GetWayPoint(j));
            }
        }

        public Vector3 GetWayPoint(int i) {
            return transform.GetChild(i).position;
        }

        public int GetNextWaypointIndex(int index) {
            return (index < transform.childCount - 1) ? (index + 1) : 0; 
        }
    }

}