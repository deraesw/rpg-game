﻿using RPG.Core;
using RPG.Movement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Controller
{
    public class AIController : MonoBehaviour
    {
        [SerializeField]
        private float _chaseDistance = 5.0f;
        [SerializeField]
        private float _waypointTolerance = 1.0f;
        [SerializeField]
        private float _suspicionTime = 5.0f;
        [SerializeField]
        private float _dwellTime = 2.0f;
        [SerializeField]
        private PatrolController _patrolController;

        private Health _health;
        private Fighter _figther;
        private GameObject _player;
        private PlayerMovement _playerMovement;

        private Vector3 _guardPosition;
        private float _timeSinceLastSeePlayer = Mathf.Infinity;
        private float _timeSinceArrivedAtWaypoint = Mathf.Infinity;
        private int _currentIndexWaypoint = 0;

        private void Start() {
            _figther = GetComponent<Fighter>();
            _health = GetComponent<Health>();
            _playerMovement = GetComponent<PlayerMovement>();
            _player = GameObject.FindGameObjectWithTag("Player");

            _guardPosition = transform.position;
        }

        private void Update() {

            if (_player == null || _health.IsDead)
                return;

            if (InAttackRangeOfPlayer() && _figther.CanAttack(_player)) {
                AttackBehaviour();
            }
            else if (_timeSinceLastSeePlayer < _suspicionTime) {
                SuspicionBehaviour();
            }
            else {
                PatrolBehaviour();
            }

            UpdateTimers();
        }

        private void UpdateTimers() {
            _timeSinceLastSeePlayer += Time.deltaTime;
            _timeSinceArrivedAtWaypoint += Time.deltaTime;
        }

        private void PatrolBehaviour() {
            Vector3 nextPosition = _guardPosition;

            if(_patrolController != null) {
                if(AtWaypoint()) {
                    _timeSinceArrivedAtWaypoint = 0;
                    CycleWaypoint();
                }
                nextPosition = GetCurrentWaypoint();
            }

            if(_timeSinceArrivedAtWaypoint > _dwellTime) {
                _playerMovement.StartMoveToDestination(nextPosition);
            }
        }

        private Vector3 GetCurrentWaypoint() {
            return _patrolController.GetWayPoint(_currentIndexWaypoint);
        }

        private void CycleWaypoint() {
            _currentIndexWaypoint = _patrolController.GetNextWaypointIndex(_currentIndexWaypoint);
        }

        private bool AtWaypoint() {
            float distance = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distance < _waypointTolerance;
        }

        private void SuspicionBehaviour() {
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AttackBehaviour() {
            _timeSinceLastSeePlayer = 0;
            _figther.Attack(_player);
        }

        private bool InAttackRangeOfPlayer() {
            float distance = Vector3.Distance(_player.transform.position, transform.position);
            return distance <= _chaseDistance;
        }

        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(transform.position, _chaseDistance);
        }
    }
}


