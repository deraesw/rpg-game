﻿using RPG.Core;
using RPG.Movement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Controller {

    public class PlayerController : MonoBehaviour {

        private Health _health;
        private PlayerMovement _playerMovement;
        private Fighter _figther;

        // Start is called before the first frame update
        void Start() {
            _playerMovement = GetComponent<PlayerMovement>();
            _figther = GetComponent<Fighter>();
            _health = GetComponent<Health>();
        }

        // Update is called once per frame
        void Update() {

            if (_health.IsDead) return;

            if (InteractWithCombat()) return;

            if (InteractWithMovement()) return;

        }

        private bool InteractWithCombat() {
            Ray ray = GetMouseRay();
            RaycastHit[] hits = Physics.RaycastAll(ray);
            foreach(RaycastHit hit in hits) {
                CombatTarget combatTarget = hit.transform.GetComponent<CombatTarget>();
                if(combatTarget != null) {
                    if(Input.GetMouseButton(0)) {
                        if(_figther.CanAttack(combatTarget.gameObject)) {
                            _figther.Attack(combatTarget.gameObject);
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        private bool InteractWithMovement() {
            return MouveToCursor();
        }

        private bool MouveToCursor() {
            Ray ray = GetMouseRay();
            if (Physics.Raycast(ray, out RaycastHit hit)) {
                if (Input.GetMouseButton(0)) {
                    _playerMovement.StartMoveToDestination(hit.point);
                }
                return true;
            }
            return false;
        }

        private static Ray GetMouseRay() {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }
    }
}

