﻿using RPG.Movement;
using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField]
        private float _weaponRange = 1.0f;
        [SerializeField]
        private float _timeBetweenAttacks = 1.0f;
        [SerializeField]
        private float _weaponDamage = 5.0f;

        private Animator _animator;
        private Health _target;
        private PlayerMovement _playerMovement;
        private ActionScheduler _actionScheduler;
        private float _timeSinceLastAttack = Mathf.Infinity;


        //Override
        private void Start() {
            _playerMovement = GetComponent<PlayerMovement>();
            _animator = GetComponent<Animator>();
            _actionScheduler = GetComponent<ActionScheduler>();
        }

        //Override
        private void Update() {
            _timeSinceLastAttack += Time.deltaTime;

            if (_target == null) return;

            if (_target.IsDead) return;

            float distance = Vector3.Distance(transform.position, _target.transform.position);
            if (distance < _weaponRange) {
                AttackBehaviour();
                _playerMovement.StopMovement();
            }
            else {
                _playerMovement.MoveToDestination(_target.transform.position);
            }

        }

        //Override - IAction
        public void Cancel() {
            ForgetTarget();
        }

        //AnimationEvent
        private void Hit() {
            if (_target != null) {
                _target.TakeDamager(_weaponDamage);
            }
        }

        private void AttackBehaviour() {
            _animator.ResetTrigger("StopAttack");
            transform.LookAt(_target.transform);
            if (_timeSinceLastAttack > _timeBetweenAttacks) {
                _animator.SetTrigger("Attack");
                _timeSinceLastAttack = 0;
            }
        }

        public void ForgetTarget() {
            _animator.SetTrigger("StopAttack");
            _target = null;
        }

        public bool CanAttack(GameObject combatTarget) {

            if (combatTarget == null) return false;

            Health targetHealth = combatTarget.GetComponent<Health>();
            return (targetHealth != null && !targetHealth.IsDead);
        }

        public void Attack(GameObject combatTarget) {
            _actionScheduler.StartAction(this);
            _target = combatTarget.GetComponent<Health>();
        }
    }

}

