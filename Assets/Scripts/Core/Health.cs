﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class Health : MonoBehaviour
    {
        private Animator _animator;
        private bool _isAlreadyDead = false;

        public bool IsDead { get{ return _isAlreadyDead;  } }

        //Overidde
        private void Start() {
            _animator = GetComponent<Animator>();
        }

        [SerializeField]
        private float _health = 100f;

        public void TakeDamager(float damage) {
            _health = Mathf.Max(_health - damage, 0);

            if(_health == 0 && !_isAlreadyDead) {
                Die();
            }
            print("Health: " + _health);
        }

        private void Die() {
            if (_isAlreadyDead) return;

            _animator.SetTrigger("Die");
            _isAlreadyDead = true;
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }
    }

}
