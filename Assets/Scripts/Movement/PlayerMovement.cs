﻿using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class PlayerMovement : MonoBehaviour, IAction
    {
        private Health _health;
        private NavMeshAgent _agent;
        private Animator _animator;
        private ActionScheduler _actionScheduler;

        // Start is called before the first frame update
        void Start() {
            _agent = GetComponent<NavMeshAgent>();
            _animator = GetComponent<Animator>();
            _actionScheduler = GetComponent<ActionScheduler>();
            _health = GetComponent<Health>();
        }

        // Update is called once per frame
        void Update() {
           
            UpdateAnimator();

            _agent.enabled = !_health.IsDead;
        }

        public void StartMoveToDestination(Vector3 destination) {
            _animator.ResetTrigger("Attack");
            _actionScheduler.StartAction(this);
            MoveToDestination(destination);
        }

        public void MoveToDestination(Vector3 destination) {
            if(_agent.enabled) {
                _agent.SetDestination(destination);
                _agent.isStopped = false;
            }
        }

        public void StopMovement() {
            if(_agent.enabled) {
                _agent.isStopped = true;
            }
        }

        private void UpdateAnimator() {
            Vector3 velocity = _agent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            _animator.SetFloat("ForwardSpeed", speed);
        }

        //Override - IAction
        public void Cancel() {
            StopMovement();
        }
    }

}

